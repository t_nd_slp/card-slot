//
//  AppDelegate.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SlotViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SlotViewController *viewController;

@end
