//
//  AppDelegate.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "AppDelegate.h"
#import "NSDate+NetworkClock.h"
#import "CoinManager.h"
#import "SlotViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    srand(time(nil));
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud registerDefaults:@{@"StockCoin":@(100)}];
    [CoinManager sharedManager].stockCoin = [ud integerForKey:@"StockCoin"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[SlotViewController alloc] initWithNibName:@"SlotViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:[NSDate networkDate] forKey:@"LastRunDate"];
    [ud setInteger:[CoinManager sharedManager].stockCoin forKey:@"StockCoin"];
    [ud synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSTimeInterval interval = [[[NSUserDefaults standardUserDefaults] objectForKey:@"LastRunDate"] timeIntervalSinceNetworkDate];
    NSLog(@"get coin %f",-interval);
    [CoinManager sharedManager].stockCoin += -(int)interval;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
