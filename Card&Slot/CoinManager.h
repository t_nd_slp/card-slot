//
//  CoinManager.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/19.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SlotDrumView.h"

@interface CoinManager : NSObject<SlotDrumDelegate>
@property int stockCoin;
+ (CoinManager*)sharedManager;
- (void) hit;
@end
