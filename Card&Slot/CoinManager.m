//
//  CoinManager.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/19.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "CoinManager.h"
#import "NSDate+NetworkClock.h"

@interface CoinManager ()

@end

@implementation CoinManager

static CoinManager* sharedCoinManager = nil;

+ (CoinManager*)sharedManager {
    @synchronized(self) {
        if (sharedCoinManager == nil) {
            sharedCoinManager = [[self alloc] init];
        }
    }
    return sharedCoinManager;
}

- (id)init
{
    self = [super init];
    if(self)
    {        
        //load coin
        //plus
        //NSLog(@"%@",[NSDate networkDate]);
    }
    return self;
}




@end
