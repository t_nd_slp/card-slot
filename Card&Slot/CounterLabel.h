//
//  CounterLabel.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/19.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CounterLabel : UILabel
@property (nonatomic, assign) int dispNumber;
-(void)setDispNumberWithAnimated:(int)dispNumber;
@end
