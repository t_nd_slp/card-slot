//
//  CounterLabel.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/19.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "CounterLabel.h"

@interface CounterLabel ()
@property int destNumber;
@property (nonatomic, weak) NSTimer *timer;
@end

@implementation CounterLabel

-(void)setDispNumber:(int)dispNumber
{
    _dispNumber = dispNumber;
    self.destNumber = dispNumber;
}


-(void)setDispNumberWithAnimated:(int)dispNumber
{
    self.destNumber = dispNumber;
    if(![self.timer isValid])
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.05f target:self selector:@selector(update) userInfo:nil repeats:YES];
}

- (void)update
{
    _dispNumber += self.dispNumber < self.destNumber ? 1 : -1;
    self.text = [NSString stringWithFormat:@"%d",self.dispNumber];
    if (self.destNumber == self.dispNumber) [self.timer invalidate];
}
@end
