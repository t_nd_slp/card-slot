//
//  SlotDrumView.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SlotDrumView;

@protocol SlotDrumDelegate <NSObject>
- (void) stoppedSlot :(SlotDrumView*)slotDrumView;
@end

@interface SlotDrumView : UIView
- (void) startSlot;
- (void) stopSlot;
- (UIImage*)getPanel:(int)panelNumber;
@property bool stopped;
@property (nonatomic,weak) id<SlotDrumDelegate> delegate;
@end
