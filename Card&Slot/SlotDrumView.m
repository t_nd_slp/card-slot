//
//  SlotDrumView.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "SlotDrumView.h"

#define MaxSpeed 20.0
#define LowSpeed 0.4
#define PositionBias 42

@interface SlotDrumView ()
@property (nonatomic, strong) NSMutableArray *panelArray;
@property (nonatomic, assign) NSTimer *timer;

@property bool inAccel;
@property float speed;
@property float accel;
@property float panelSize;
@end

@interface NSMutableArray (randomized)
- (NSMutableArray *) randomizedArray;
@end

@implementation SlotDrumView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
        self.panelSize = self.bounds.size.width;
        
        self.panelArray = [[NSMutableArray alloc] initWithObjects:
                           [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico1.png"]],
                           [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico2.png"]],
                           [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico3.png"]],
                           [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico4.png"]],
                           [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico5.png"]], nil];
        [self.panelArray randomizedArray];
        
        for(int i=0; i<self.panelArray.count; i++)
        {
            UIImageView *panel = self.panelArray[i];
            float width = self.bounds.size.width;
            panel.frame = CGRectMake(0, width*i, width, width);
            panel.tag = i;

            [self addSubview:panel];
        }

    }
    return self;
}

- (void) startSlot
{
    self.accel = 0.1;
    self.speed = 0;
    self.inAccel = YES;
    self.stopped = NO;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01f target:self selector:@selector(update) userInfo:nil repeats:YES];
}

- (void)stopSlot
{
    self.inAccel = NO;
}

- (void)update
{
    //一番上のパネル
    UIImageView *headPanel = (UIImageView*)self.panelArray[0];
    
    //加速
    if (self.inAccel && self.speed<MaxSpeed) self.speed += self.accel;
    //減速
    else if(self.speed > 0.0)
    {
        self.speed -= self.accel;
        
        if(self.speed<LowSpeed)
        {
            self.speed = LowSpeed;
            //ぴったり止める
            if( -self.panelSize/4 < headPanel.center.y && headPanel.center.y < -self.panelSize/4+1)
            {
                self.speed = 0;
                self.stopped = YES;
                [self.timer invalidate];//releaseまでされる
                if([self.delegate respondsToSelector:@selector(stoppedSlot:)])
                    [self.delegate stoppedSlot:self];
            }
        }
    }

    headPanel.center = CGPointMake(self.panelSize/2, headPanel.center.y + self.speed);
    
    //各パネルの相対位置更新
    for(int i=1; i<self.panelArray.count; i++)
    {
        UIImageView *panel = (UIImageView*)self.panelArray[i];
        panel.center = CGPointMake(self.panelSize/2, headPanel.center.y + self.panelSize*i );
        
        //headに並び直し
        if(panel.center.y + PositionBias > self.panelArray.count * self.panelSize)
        {
            UIImageView *last = self.panelArray[self.panelArray.count-1];
            last.center = CGPointMake(self.panelSize/2, -PositionBias);
            [self.panelArray insertObject:last atIndex:0];
            [self.panelArray removeLastObject];
        }
    }

}

-(UIImage *)getPanel:(int)panelNumber
{
    if(self.speed != 0.0) return nil;
    else return [(UIImageView*)self.panelArray[panelNumber] image];
}

@end

#pragma mark randomize algorhythm

@implementation NSMutableArray (randomized)
- (NSMutableArray *) randomizedArray {
    int i = [self count];
    
    while(--i) {
        int j = rand() % (i+1);
        [self exchangeObjectAtIndex:i withObjectAtIndex:j];
    }
    return self;
}
@end
