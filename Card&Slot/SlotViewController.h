//
//  ViewController.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlotDrumView.h"

@interface SlotViewController : UIViewController<SlotDrumDelegate>

@end
