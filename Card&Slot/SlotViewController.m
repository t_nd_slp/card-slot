//
//  ViewController.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import "SlotViewController.h"
#import "CoinManager.h"
#import "CounterLabel.h"
#import "OALSimpleAudio.h"

@interface SlotViewController ()
@property (weak, nonatomic) IBOutlet SlotDrumView *slotDrum0;
@property (weak, nonatomic) IBOutlet SlotDrumView *slotDrum1;
@property (weak, nonatomic) IBOutlet SlotDrumView *slotDrum2;
@property (weak, nonatomic) IBOutlet UIButton *stopButton0;
@property (weak, nonatomic) IBOutlet UIButton *stopButton1;
@property (weak, nonatomic) IBOutlet UIButton *stopButton2;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet CounterLabel *stockCoinLabel;
@property (weak, nonatomic) CoinManager *coinManager;

@end

@implementation SlotViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.coinManager = [CoinManager sharedManager];
    self.stockCoinLabel.dispNumber = self.coinManager.stockCoin;
    self.slotDrum0.delegate = self;
    self.slotDrum1.delegate = self;
    self.slotDrum2.delegate = self;
    
}

- (IBAction)startSlotDrum:(id)sender
{
    if( !self.startButton.selected )
    {
        self.coinManager.stockCoin -= 5;
        [self.stockCoinLabel setDispNumberWithAnimated:self.coinManager.stockCoin];

        [self.slotDrum0 startSlot];
        [self.slotDrum1 startSlot];
        [self.slotDrum2 startSlot];
        self.startButton.selected = YES;
        self.stopButton0.selected = NO;
        self.stopButton1.selected = NO;
        self.stopButton2.selected = NO;
        self.stopButton0.enabled = NO;
        self.stopButton1.enabled = NO;
        self.stopButton2.enabled = NO;
        [[OALSimpleAudio sharedInstance] playEffect:@"mecha35.wav"];
        [[OALSimpleAudio sharedInstance] playBg:@"nc44420.wav" loop:YES];
        
        [self performSelector:@selector(enableStopButtons) withObject:self afterDelay:1.0];
    }
}

- (void)enableStopButtons
{
    self.stopButton0.enabled = YES;
    self.stopButton1.enabled = YES;
    self.stopButton2.enabled = YES;
}


- (IBAction)stopSlotDrum:(id)sender
{
    if([sender isEqual:self.stopButton0]) {[self.slotDrum0 stopSlot]; self.stopButton0.selected = YES;}
    if([sender isEqual:self.stopButton1]) {[self.slotDrum1 stopSlot]; self.stopButton1.selected = YES;}
    if([sender isEqual:self.stopButton2]) {[self.slotDrum2 stopSlot]; self.stopButton2.selected = YES;}
    [[OALSimpleAudio sharedInstance] playEffect:@"cursor15_a.wav"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)stoppedSlot:(SlotDrumView *)slotDrumView
{
    [[OALSimpleAudio sharedInstance] playEffect:@"set.wav"];
    
    
    //hit
    if( ([[self.slotDrum0 getPanel:2] isEqual:[self.slotDrum1 getPanel:2]] &&   //横
         [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:2]] ) ||
        ([[self.slotDrum0 getPanel:1] isEqual:[self.slotDrum1 getPanel:1]] &&
         [[self.slotDrum1 getPanel:1] isEqual:[self.slotDrum2 getPanel:1]] ) ||
        ([[self.slotDrum0 getPanel:3] isEqual:[self.slotDrum1 getPanel:3]] &&
         [[self.slotDrum1 getPanel:3] isEqual:[self.slotDrum2 getPanel:3]] ) ||
        ([[self.slotDrum0 getPanel:1] isEqual:[self.slotDrum1 getPanel:2]] &&    //斜め
         [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:3]] ) ||
        ([[self.slotDrum0 getPanel:3] isEqual:[self.slotDrum1 getPanel:2]] &&
         [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:1]] ) )
    {
        NSLog(@"Hit!");
        [[OALSimpleAudio sharedInstance] playEffect:@"nc41828.wav"];
        self.coinManager.stockCoin += 50;
        [self.stockCoinLabel setDispNumberWithAnimated:self.coinManager.stockCoin];

    }
    
    //all stop
    if( self.slotDrum0.stopped && self.slotDrum1.stopped && self.slotDrum2.stopped )
    {
        [[OALSimpleAudio sharedInstance] stopBg];
        self.startButton.selected = NO;
        return;
    }
    
    //reach
    if( ([[self.slotDrum0 getPanel:2] isEqual:[self.slotDrum1 getPanel:2]] ||   //横
         [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:2]] ) ||
       ([[self.slotDrum0 getPanel:1] isEqual:[self.slotDrum1 getPanel:1]] ||
        [[self.slotDrum1 getPanel:1] isEqual:[self.slotDrum2 getPanel:1]] ) ||
       ([[self.slotDrum0 getPanel:3] isEqual:[self.slotDrum1 getPanel:3]] ||
        [[self.slotDrum1 getPanel:3] isEqual:[self.slotDrum2 getPanel:3]] ) ||
       ([[self.slotDrum0 getPanel:1] isEqual:[self.slotDrum1 getPanel:2]] ||    //斜め
        [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:3]] ) ||
       ([[self.slotDrum0 getPanel:3] isEqual:[self.slotDrum1 getPanel:2]] ||
        [[self.slotDrum1 getPanel:2] isEqual:[self.slotDrum2 getPanel:1]] ) )
    {
        [[OALSimpleAudio sharedInstance] playEffect:@"reach.wav"];
    }
}

@end
