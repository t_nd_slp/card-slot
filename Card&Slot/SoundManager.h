//
//  SoundManager.h
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/20.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
// plistに書いてあるkeyと対応した音を鳴らす

#import <Foundation/Foundation.h>

@interface SoundManager : NSObject

+ (SoundManager*)sharedManager;
- (void) play:(NSString*)key loop:(bool)loop;
- (void) stop:(NSString*)key;
@property float volume;
@end
