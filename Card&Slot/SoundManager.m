//
//  SoundManager.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/20.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
// 

#import "SoundManager.h"
#import <AVFoundation/AVFoundation.h>

@interface SoundManager ()
@property (nonatomic, strong) NSMutableDictionary *SoundPlayers;
@property SystemSoundID beepSoundId;
@end

@implementation SoundManager

static SoundManager* sharedSOundManager = nil;

+ (SoundManager*)sharedManager {
    @synchronized(self) {
        if (sharedSOundManager == nil) {
            sharedSOundManager = [[self alloc] init];
        }
    }
    return sharedSOundManager;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.SoundPlayers = [NSMutableDictionary dictionary];
        NSString *p = [[NSBundle mainBundle] pathForResource:@"SoundList" ofType:@"plist"];
        NSDictionary *soundList = [NSDictionary dictionaryWithContentsOfFile:p];
        
        for (int i=0; i<[soundList count]; i++)
        {
            NSString *key = [soundList allKeys][i];
            AVAudioPlayer *player = [self getAVAudioPlayer:[soundList objectForKey:key]];
            [player prepareToPlay];
            
            [self.SoundPlayers addEntriesFromDictionary:@{key : player}];
        }
    }
    return self;
}

- (AVAudioPlayer*)getAVAudioPlayer:(NSString*)soudFileName
{
    AVAudioPlayer *player=nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:soudFileName ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    if(url){
        NSError *err = nil;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&err];
    }
    
    return player;
}

- (void) play:(NSString*)key loop:(bool)loop
{
    AVAudioPlayer *player = [self.SoundPlayers objectForKey:key];
    if(player!=nil) {
        if(loop) player.numberOfLoops = -1;
        if([player isPlaying]) [player stop];
        [player play];
    }
}

- (void) stop:(NSString*)key
{
    AVAudioPlayer *player = [self.SoundPlayers objectForKey:key];
    if(player!=nil && [player isPlaying]) [player stop];
}

@end
