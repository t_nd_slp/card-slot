//
//  main.m
//  Card&Slot
//
//  Created by コマツバラ ヒロシ on 13/01/18.
//  Copyright (c) 2013年 コマツバラ ヒロシ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
